import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart'; 
import 'package:giffy_dialog/giffy_dialog.dart';



class LanguagePage extends StatefulWidget {
    static final String routeName = 'language';
    LanguagePage({Key key}) : super(key: key);

    _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {

    @override
    Widget build(BuildContext context) {
        var dataLocalization = EasyLocalizationProvider.of(context).data;
        // print("==============================");
        // print(dataLocalization.locale);

        return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
                title: Text(
                    'language.title',
                    style: TextStyle(
                        fontFamily: "Gotik",
                        fontWeight: FontWeight.w600,
                        fontSize: 18.5,
                        letterSpacing: 1.2,
                        color: Colors.black87

                    ),
                ).tr(),
                centerTitle: true,
                iconTheme: IconThemeData(color: Color(0xFF6991C7)),
                elevation: 0.0,
            ),
            body: SingleChildScrollView(
                child: Column(
                    children: <Widget>[
                        InkWell(
                            onTap: (){
                                showDialog(context: context,builder: (_) => NetworkGiffyDialog(
                                    image: Image.asset("assets/flag.png",fit: BoxFit.cover),
                                    title: Text(
                                        'titleCard',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: "Gotik",
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.w600
                                        )
                                    ).tr(),
                                    description:Text(
                                        'descCard',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: "Popins",
                                            fontWeight: FontWeight.w300,
                                            color: Colors.black26
                                        ),
                                    ).tr(),
                                    onOkButtonPressed: () {
                                        print('start');
                                        print(dataLocalization.locale.toString());
                                        this.setState(() {
                                            dataLocalization.changeLocale(Locale('ar', 'DZ'));
                                            print("Language was changed to: " + Locale('ar', 'DZ').toString() +" reality:"+ dataLocalization.locale.toString());
                                            print('Changed to arabic');
                                        });
                                        print(dataLocalization.locale.toString());
                                        print('end');
                                        Navigator.pop(context);
                                    },
                                ));
                            },
                            child: CardName(
                                flag: "assets/flag.png",
                                title: tr('language.spanish')
                            )
                        ),
                        InkWell(
                            onTap: (){
                                showDialog(context: context,builder: (_) => NetworkGiffyDialog(
                                    image: Image.asset("assets/flag.png",fit: BoxFit.cover,),
                                    title: Text(
                                        'titleCard',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: "Gotik",
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.w600
                                        )
                                    ).tr(),
                                    description:Text(
                                        'descCard',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: "Popins",
                                            fontWeight: FontWeight.w300,
                                            color: Colors.black26
                                        ),
                                    ).tr(),
                                    onOkButtonPressed: () {
                                        print('start');
                                        print(dataLocalization.locale.toString());
                                        this.setState(() {
                                            dataLocalization.changeLocale(Locale('en', 'US'));
                                            print("Language was changed to: " + Locale('en', 'US').toString() +" reality:"+ dataLocalization.locale.toString());
                                            print('Changes to english');
                                        });
                                        print('end');
                                        print(dataLocalization.locale.toString());
                                        Navigator.pop(context);        
                                    },
                                ));
                            },
                            child: CardName(
                                flag: "assets/flag.png",
                                title: tr('language.english')
                            )
                        ),
                        SizedBox(height: 70.0)
                    ],
                ),
            ),
        );
    }
}

class CardName extends StatelessWidget {
    String title, flag;
    CardName({this.title,this.flag});

    @override
    Widget build(BuildContext context) {
        return Padding(
            padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 20.0),
            child: Container(
                height: 80.0,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                    boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 10.0,
                            spreadRadius: 0.0
                        )
                    ]
                ),
                child: Padding(
                    padding: const EdgeInsets.only(left:20.0,right: 15.0),
                    child: Row(
                        children: <Widget>[
                            Image.asset(flag,fit: BoxFit.cover,height: 65.0),
                            Padding(
                            padding: const EdgeInsets.only(left:20.0,right: 20.0),
                            child: Text(
                                title,
                                style:TextStyle(
                                    fontFamily: "Popins",
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 1.3)
                                ),
                            )
                        ]
                    ),
                ),
            ),
        );
    }
}