import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:node_vpnui/language_page.dart';




void main() async{

    WidgetsFlutterBinding.ensureInitialized();


    runApp( EasyLocalization(child: MyApp()) );
}

class MyApp extends StatelessWidget {

    @override
    Widget build(BuildContext context) {
        var data = EasyLocalizationProvider.of(context).data;

        // Portrait
        SystemChrome.setPreferredOrientations([
            DeviceOrientation.portraitUp,
            DeviceOrientation.portraitDown,
        ]);
        // Color status bar
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
            statusBarColor: Colors.transparent,
        ));

        return EasyLocalizationProvider(
            data: data,
            child: MaterialApp(
                debugShowCheckedModeBanner: false,
                localizationsDelegates: [
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate,
                    EasyLocalizationDelegate(
                        locale: data.locale,
                        path: 'assets/i18n',
                    ),
                ],
                supportedLocales: [
                  Locale('en','US'),
                  Locale('ar','DZ'),
                    
                ],
                locale: data.locale,
                theme: ThemeData(
                    brightness: Brightness.light,
                    backgroundColor: Colors.white,
                    primaryColorLight: Colors.white,
                    primaryColorBrightness: Brightness.light,

                    primaryColor: Color(0xFFf5851f),
                    primaryIconTheme: IconThemeData(color: Colors.white),
                ),
                title: 'My app',
                home: LanguagePage(),
            ),
        );
    }
}